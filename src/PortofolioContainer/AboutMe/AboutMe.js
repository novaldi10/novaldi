import React from 'react';
import './AboutMe.css';
import tentang from '../../assets/Home/cwo.jpg';
import penghargaan from '../../assets/Home/reward.png';

export default function AboutMe() {
    return (
        <div className='a'>
            <div className='a-left'>
                <div className='a-card bg'></div>
                <div className='a-card'>
                    <img 
                        src={tentang}
                        alt='not responding'
                        className='a-img'
                    />
                </div>
            </div>
            <div className='a-right'>
                <h1 className='a-title' id='tentang'>Tentang Saya</h1>
                <p className='a-sub'>
                    Saya adalah seseorang bertugas untuk membuat dan mengembangkan suatu sistem, aplikasi, atau perangkat lunak dengan menggunakan bahasa pemrograman sederhana.
                </p>
                <p className='a-desc'>
                    Programmer adalah sebuah jenis profesi atau pekerjaan yang bertujuan untuk membuat sebuah sistem menggunakan bahasa pemrograman. Seseorang yang memiliki skill menulis kode program (syntax) dan merancang sistem, bisa juga disebut programmer. Kode atau bahasa program yang dimaksud seperti Java, Python, Javascript, PHP, dll.
                    Sistem yang sering kamu gunakan sehari-hari, seperti web, aplikasi Android, sistem operasi (Windows, Linux, iOS) dll, itu semua dibuat dengan bahasa pemrograman yang disusun oleh para programmer. 
                </p>
                <div className='a-award'>
                    <img src={penghargaan} alt="loh" className='a-award-img'/>
                    <div className='a-award-texts'>
                        <h4 className='a-award-title'>Webinar Basis Data</h4>
                        <p className='a-award-desc'>
                            Data Manajemen, Data Information, Data Manajement Challenges, Data Manajement Strategy, Data Manajemen Framework, Data Handing Ethics
                        </p>
                    </div>
                </div>    
            </div>
        </div>
    );
}